/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.bookstore.controller.user;

import com.bookstore.dal.impl.BookDAO;
import com.bookstore.entity.Book;
import com.bookstore.entity.ItemCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ADMIN
 */
public class CartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/user/cart/cartDetails.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null || action.isEmpty()) {
            response.sendRedirect("home");
        }

        switch (action) {
            case "add-product":
                addProduct(request, response);
                response.sendRedirect("cart");
                break;
            case "change-quantity":
                changeQuantity(request, response);
                response.sendRedirect("cart");
                break;
            case "delete":
                deleteItem(request, response);
                response.sendRedirect("cart");
                break;
            default:
                response.sendRedirect("home");
        }

    }

    private void addProduct(HttpServletRequest request, HttpServletResponse response) {
        BookDAO dao = new BookDAO();
        HttpSession session = request.getSession();

        //get quantity
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        //get id
        int id = Integer.parseInt(request.getParameter("id"));

        //find product by id
        Book book = dao.findOneById(id);
        //tao ra 1 doi tuong ItemCart
        ItemCart itemCart = new ItemCart(book, quantity);

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        //kiem tra neu chua tung co cart
        if (cart == null) {
            cart = new HashMap<>();
        }
        //them ItemCart vao ben trong cart
        addOrderDetails(itemCart, cart, quantity);
        //luu tru cart len session
        session.setAttribute("cart", cart);
    }

    private void addOrderDetails(ItemCart itemCart, HashMap<Integer, ItemCart> cart,
            int quantity) {
        int idProductInOrderDetails = itemCart.getBook().getId();
        //kiem tra xem trong cart da tung co sn pham hay chua
        //neu da co san pham roi, thi + don` so luong
        if (cart.containsKey(idProductInOrderDetails)) {
            int oldQUantity = cart.get(idProductInOrderDetails).getQuantity();
            ItemCart orderDetailsInCart = cart.get(idProductInOrderDetails);
            orderDetailsInCart.setQuantity(oldQUantity + quantity);
            //neu chua thi moi mang di put
        } else {
            cart.put(idProductInOrderDetails, itemCart);
        }

    }

    private void changeQuantity(HttpServletRequest request, HttpServletResponse response) {
        BookDAO dao = new BookDAO();
        HttpSession session = request.getSession();
        //get id
        int id = Integer.parseInt(request.getParameter("id"));
        //get quantity
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        //find product by id
        Book book = dao.findOneById(id);

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        //kiem tra neu chua tung co cart
        if (cart == null) {
            cart = new HashMap<>();
        }
        //change quantity of product
        ItemCart itemCart = cart.get(id);
        itemCart.setQuantity(quantity);
        //luu tru cart len session
        session.setAttribute("cart", cart);

    }

    private void deleteItem(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //get id
        int id = Integer.parseInt(request.getParameter("id"));

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        cart.remove(id);
        //luu tru cart len session
        session.setAttribute("cart", cart);
    }

}
