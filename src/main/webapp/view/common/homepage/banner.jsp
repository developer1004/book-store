<%-- 
    Document   : banner
    Created on : Jun 1, 2023, 8:01:42 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            img {
                border-radius: 12px;
            }

            .carousel-control-prev,
            .carousel-control-next {
                /*background-color: #000000;*/
            }

        </style>
    </head>
    <body>
        <section id="banner" class="container-fluid" >
            <!-- Thẻ Carousel -->
            <div id="carouselExample" class="carousel slide" data-ride="carousel" style="background-color: #ffd400">
                <!-- Các chỉ số carousel -->
                <ol class="carousel-indicators">
                    <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExample" data-slide-to="1"></li>
                    <li data-target="#carouselExample" data-slide-to="2"></li>
                </ol>

                <!-- Các ảnh carousel -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <!-- Ảnh 1 -->
                        <img src="https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/2023/07/banner/Lap-720-220-720x220-1.png" alt="Image 1">
                    </div>
                    <div class="carousel-item">
                        <!-- Ảnh 2 -->
                        <img src="https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/2023/07/banner/iP14PRM-720-220-720x220.png" alt="Image 2">
                        <div class="carousel-caption">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!-- Ảnh 3 -->
                        <img src="https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/2023/07/banner/A14-720-220--1--720x220.png" alt="Image 3">
                    </div>
                    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <!-- Các nút điều khiển carousel -->
            </div>
        </div>
    </section>

</body>
</html>
